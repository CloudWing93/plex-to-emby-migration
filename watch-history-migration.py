from plexapi.myplex import MyPlexAccount

import os
import json
import argparse
import requests
from urllib.parse import urlencode
from requests import Session
from requests.adapters import HTTPAdapter
from requests.exceptions import RequestException

VERIFY_SSL = False

PLEX_SERVER_NAME = "SomeFriendlyServerName"
PLEX_MOVIE_LIBRARIES = "Movies"
PLEX_SERIES_LIBRARIES = "TV Shows"
EMBY_URL = "http://localhost:8096/emby/"
EMBY_APIKEY = "xxxxxxxx"

## The plex token should be for the user you are trying to migrate history for.
# Check out the readme for more info
PLEX_ADMIN_USER_TOKEN = "xxxxxxxx"
EMBY_ADMIN_USERNAME = ""


# If plex home user doesnt have a pin just leave it blank. If you don't have any home users just leave it set as: HOME_USER_MAPPINGS = []
HOME_USER_MAPPINGS = [
    {"plexUsername": "test1", "plexPin": "1111", "embyUsername": "test1"},
    {"plexUsername": "test2", "plexPin": "", "embyUsername": "test2"}
]



# ----------------- [Do not edit below this line] ----------------- 
class emby:
    def __init__(self, url, apikey, verify_ssl=False, debug=None):
        self.url = url
        self.apikey = apikey
        self.debug = debug

        self.session = Session()
        self.adapters = HTTPAdapter(max_retries=3,
                                    pool_connections=1,
                                    pool_maxsize=1,
                                    pool_block=True)
        self.session.mount('http://', self.adapters)
        self.session.mount('https://', self.adapters)

        # Ignore verifying the SSL certificate
        if verify_ssl is False:
            self.session.verify = False
            # Disable the warning that the request is insecure, we know that...
            import urllib3
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _call_api(self, cmd, method='GET', payload=None, data=None):
        params = (
            ('api_key', self.apikey),
        )

        if payload:
            params = params + payload

        headers = {
            'accept': '',
            'Content-Type': 'application/json',
        }

        if method == 'GET':
            try:
                response = requests.get(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                print("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        elif method == 'POST':
            try:
                response = requests.post(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                print("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        if response.status_code != 204:
            try:
                response_json = json.loads(response.text)
            except ValueError:
                print(
                    "Failed to parse json response for Emby API cmd '{}': {}"
                    .format(cmd, response.content))
                return
        if response.status_code == 200:
            if self.debug:
                print("Successfully called Emby API cmd '{}'".format(cmd))
            return response_json
        elif response.status_code == 204:
            if self.debug:
                print("Successfully called Emby API cmd '{}'".format(cmd))
        else:
            error_msg = response.reason
            print("Emby API cmd '{}' failed: {}".format(cmd, error_msg))
            return

    def get_emby_movies(self, userID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "IncludeItemTypes": "Movie", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_shows(self, userID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "IncludeItemTypes": "Series", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_show_by_name(self, userID=None, showName=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "SearchTerm": showName, "IncludeItemTypes": "Series", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_movie_by_provider_id(self, providerId=None):
        path = f"/Items?"
        params = {"Recursive": "true",'AnyProviderIdEquals': providerId, 'Fields': 'ProviderIds', 'IncludeItemTypes': 'Movie'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_series_by_provider_id(self, providerId=None):
        path = f"/Items?"
        params = {"Recursive": "true",'AnyProviderIdEquals': providerId, 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_episodes(self, userID=None, parentID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "ParentId": parentID, "IncludeItemTypes": "Episode", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def set_emby_played(self, userID=None, videoID=None):
        payload = ()
        data = "{\'Played\':\'True\'}"
        path = f"/Users/{userID}/PlayedItems/{videoID}"
        return self._call_api(path, 'POST', payload, data)
    def GetEmbyIDFromUsername(self, username):
        path = f"/Users"
        params = {}
        cmd = path + urlencode(params)
        users = self._call_api(cmd, 'GET')
        for u in users:
            if u['Name'] == username:
                return u['Id']
        return None
    
def migrateMovieHistory(plexClient, embyUserName):
    print(f"\nStarting Movie Matching for user: {embyUserName}.")
    embyClient = emby(EMBY_URL.rstrip('/'), EMBY_APIKEY, VERIFY_SSL)
    embyUserId = embyClient.GetEmbyIDFromUsername(embyUserName)
    plexMovieLibraries = PLEX_MOVIE_LIBRARIES.split(",")
    for library in  plexMovieLibraries:
        for plexMovie in plexClient.library.section(library).search(unwatched=False):
                embyWatched = False
                

                for id in plexMovie.guids:
                    provider = id.id.split('://')
                    em = embyClient.get_emby_movie_by_provider_id(f"{provider[0]}.{provider[1]}")
                    
                    
                    if len(em['Items']) > 0 and embyWatched == False:
                        for m in em['Items']:
                            embyClient.set_emby_played(embyUserId, m["Id"])
                            embyWatched = True
                            print(f"movie match success using {provider[0]} -- {m['Name']}")

                if embyWatched == False:
                    unmatchedMovies.append(f"{embyUserName} - plexMovie.title")

def migrateShowsHistory(plexClient, embyUserName):
    print(f"\nStarting TV Series Matching for user: {embyUserName}.")
    embyClient = emby(EMBY_URL.rstrip('/'), EMBY_APIKEY, VERIFY_SSL)
    embyUserId = embyClient.GetEmbyIDFromUsername(embyUserName)
    plexSeriesLibraries = PLEX_SERIES_LIBRARIES.split(",")
    for library in  plexSeriesLibraries:
        for plexShow in plexClient.library.section(library).search(unwatched=False):
            plexSeasons = plexShow.seasons()
            plexEpisodes = plexShow.episodes()

            for pe in plexEpisodes:
                if pe.isPlayed == True:
                    embyWatched = False
                    for id in pe.guids:
                        provider = id.id.split('://')
                        embyEpisodes = embyClient.get_emby_series_by_provider_id(f'{provider[0]}.{provider[1]}')
                        for ee in embyEpisodes['Items']:
                            if embyWatched == False:
                                embyClient.set_emby_played(embyUserId, ee["Id"])
                                embyWatched = True
                                print(f"Episode match success using {provider[0]} -- {plexShow.title} - {pe.seasonEpisode} - {pe.title}")

                if embyWatched == False:
                    unmatchedSeries.append(f"{embyUserName} - {plexShow.title} - {pe.seasonEpisode} - {pe.title}")


unmatchedMovies = []
unmatchedSeries = []

adminAccount = MyPlexAccount(token=PLEX_ADMIN_USER_TOKEN)
plexClientAdminUser = adminAccount.resource(PLEX_SERVER_NAME).connect()
migrateMovieHistory(plexClientAdminUser, EMBY_ADMIN_USERNAME)
migrateShowsHistory(plexClientAdminUser, EMBY_ADMIN_USERNAME)

for user in HOME_USER_MAPPINGS:
    if user['plexPin'] == "":
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'])
    else:
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'], pin=user['plexPin'])
    plexClientHomeUser = account.resource(PLEX_SERVER_NAME).connect()
    migrateMovieHistory(plexClientHomeUser, user['embyUsername'])
    migrateShowsHistory(plexClientHomeUser, user['embyUsername'])

if len(unmatchedMovies) > 0:
    print("\nMatch NOT found for the following Movies:")
    for i in unmatchedMovies:
        print(i)
if len(unmatchedSeries) > 0:
    print("\nMatch NOT found for the following Series:")
    for i in unmatchedSeries:
        print(i)